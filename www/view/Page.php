<?php
namespace view;

class Page
{
    public function displayPage() 
    {
        $this->headerTag();
        $this->menuTag();
        $this->contentTag();
        $this->footerTag();
    }

    public function headerTag() 
    {
        include "include/header.php";
    } 

    public function menuTag() 
    {
        include "include/menu.php";
    }

    protected function contentTag() 
    {
        
    }
    
    public function footerTag() 
    {
        include "include/footer.php";
    }
}

