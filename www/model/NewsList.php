<?php
namespace model;
use PDO;
use model\DataManager;

class NewsList
{
    protected $newsList = null;    
    protected $conn = null;
    protected $dm = null;
    
    public function __construct()
    {
        $this->newsList = array();
        $this->dm = new DataManager;
        $this->conn = $this->dm->connect();
    }
    public function __destruct()
    {
        $this->dm->disconnect($this->conn);
        unset($dm);
    }
    public function getList()
    {
        $result =  $this->conn->prepare("select * from news");
        $result->execute(); 
        $num_news = $result->rowCount();   
        for ($i = 0; $i < $num_news; $i++) {
            $this->newsList[$i] = new News;            
            $news[$i] = $result->fetch(PDO::FETCH_OBJ);            
            $this->newsList[$i]->setId($news[$i]->id); 
            $this->newsList[$i]->setSource($news[$i]->source);
            $this->newsList[$i]->setTitle($news[$i]->title);
            $this->newsList[$i]->setSummary($news[$i]->summary);
            $this->newsList[$i]->setImg($news[$i]->img);
            $this->newsList[$i]->setVkRef($news[$i]->vkRef);
            $this->newsList[$i]->setFbRef($news[$i]->fbRef);
        }
    }
    
    public function printList() 
    {
        $num = count($this->newsList);
        for ($i = 0; $i < $num; $i++) {
            echo "<p>";
                echo ($i + 1).". ";
                $this->newsList[$i]->printNews();
            echo "</p>";
        }
    }
    
    public function sortList()
    {
        $t = true;
        while ($t) {
            $t = false;
            for ($i = 0; $i < count($this->newsList) - 1; $i++) {
                if (($this->newsList[$i]->getFbRef() + $this->newsList[$i]->getVkRef()) < ($this->newsList[$i + 1]->getFbRef() + $this->newsList[$i + 1]->getVkRef())) {
                    $temp = $this->newsList[$i + 1];
                    $this->newsList[$i + 1] = $this->newsList[$i];
                    $this->newsList[$i] = $temp;
                    $t = true;
                }
            }
        }
    
    }
}

