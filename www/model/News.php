<?php
namespace model;
use PDO;
use model\DataManager;

class News 
{
    protected $id = null;
    protected $source = null;
    protected $title = null;
    protected $summary = null;
    protected $img = null;
    protected $vkRef = null;
    protected $fbRef = null;
    protected $conn = null;
    protected $dm = null;
    
    public function __construct()
    {
        $this->dm = new DataManager;
        $this->conn = $this->dm->connect();
    }
     public function __destruct()
    {
        $this->dm->disconnect($this->conn);
        unset($dm);
    }
    public function setId($id)
    {
        $this->id = $id;  
    }
    public function setSource($source) 
    {
        $this->source = $source; 
    }
    public function setTitle($title) 
    { 
        $this->title = $title; 
    }
    public function setSummary($summary)
    { 
        $this->summary = $summary;  
    }
    public function setImg($img) 
    {
        $this->img = $img;  
    }
    public function setVkRef($vkRef)   
    { 
        $this->vkRef = $vkRef;  
    }
    public function setFbRef($fbRef)  
    {
        $this->fbRef = $fbRef; 
    }        
    public function getId() 
    { 
    return  $this->id;
    }
    public function getSource() 
    {
        return  $this->source;
    }    
    public function getTitle() 
    {
        return  $this->title;  
    }
    public function getSummary() 
    {
        return  $this->summary;
    }
    public function getImg() 
    {
        return  $this->img; 
    }
    public function getVkRef() 
    { 
        return  $this->vkRef; 
    }
        public function getFbRef()  
    { 
        return  $this->fbRef; 
    }

    public function setNews()
    {
        if($this->source) {
            $result = $this->conn->prepare("select * from news where source=:source");
            $result->bindValue(':source', $this->source, PDO::PARAM_STR);
            $result->execute();
            $num = $result->rowCount();
            if(!$num) {
                $sht = $this->conn->prepare("INSERT INTO news (source, title, summary, img, vkRef, fbRef) VALUES (:source, :title, :summary, :img, :vkRef, :fbRef)");
                $sht->bindValue(':source', $this->source, PDO::PARAM_STR);
                $sht->bindValue(':title', $this->title, PDO::PARAM_STR);
                $sht->bindValue(':summary', $this->summary, PDO::PARAM_STR);
                $sht->bindValue(':img', $this->img, PDO::PARAM_STR);
                $sht->bindValue(':vkRef', $this->vkRef, PDO::PARAM_INT);
                $sht->bindValue(':fbRef', $this->fbRef, PDO::PARAM_INT);  
                $sht->execute();
            }              
        }
    }
    public function getNews($link)    
    {        
        $result = $this->conn->prepare("select * from news where source=:source");
        $result->bindValue(':source', $link, PDO::PARAM_STR);
        $result->execute();        
        $news = $result->fetch(PDO::FETCH_OBJ);    
        $this->id = $news->id;
        $this->source = $news->source;
        $this->title = $news->title;
        $this->summary = $news->summary;
        $this->img = $news->img;
        $this->vkRef = $news->vkRef;
        $this->fbRef = $news->fbRef;  
    }
    public function printNews() 
    {
        if($this->source) {                
            echo "<a href=".$this->source.">".$this->title."</a><br/>";
            echo $this->summary."<br/>";
            if($this->img)
                echo "<img src=\"".$this->img."\" width=\"30%\"><br/>";            
            echo "Упоминаний: ".($this->vkRef + $this->fbRef);                                    
        }
    }            
}

