<?php
namespace model;
use PDO;
use PDOException;

class DataManager
{   
    public function connect() {
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=database', "root", "");
        } catch  (PDOException $e) {
            printf("Не удалось подключиться: %s\n", $e);
            die();
        } 
        return $dbh;
    }
    public function disconnect($conn) {
      $dbh = null;
    }
}
