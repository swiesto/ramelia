<?php
namespace model;

class NewsRG extends News
{
    public function loadNews($link)
    {
        $text = null;
        $this->source = $link;        
        $ch = curl_init($link);    
        curl_setopt ($ch , CURLOPT_USERAGENT , "Chrome/40.0"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_ENCODING, 'utf-8;');
        $text = curl_exec($ch);                
        curl_close($ch);
         
        $tagStart = "title";
        $posStart = stripos($text, $tagStart) + strlen($tagStart) + 1;
        $posEnd = stripos($text, "/".$tagStart) - 1;        
        $temp = substr($text, $posStart, ($posEnd - $posStart));
        $posEnd = stripos($temp, " Российская газета") - 8;        
        $this->title = substr($temp, 0, $posEnd);        
        
        $tagStart = 'article';
        $tagEnd = 'data-ga-event';                        
        $posStart = stripos($text, $tagStart) + strlen($tagStart);
        $posEnd = stripos($text, $tagEnd);            
        $temp = substr($text, $posStart, ($posEnd - $posStart));                    
        $tagStart = '<p>';
        $tagEnd = '</p>';                
        $posStart = stripos($temp, $tagStart) + strlen($tagStart);
        $posEnd = stripos($temp, $tagEnd);                
        $summary = substr($temp, $posStart, ($posEnd - $posStart));    
        $this->summary = $summary;
        
        $tagStart = 'rgb_material-img_art';
        $tagEnd = 'b-material-img__img';                
        $t = stripos($text, $tagStart);
        if($t) {                
            $posStart = stripos($text, $tagStart) + strlen($tagStart);
            $posEnd = stripos($text, $tagEnd);                
            $temp = substr($text, $posStart, ($posEnd - $posStart));                            
            $tagStart = 'src';
            $tagEnd = 'jpg';                
            $posStart = stripos($temp, $tagStart) + strlen($tagStart) + 2;
            $posEnd = stripos($temp, $tagEnd) + 3;     
            $this->img = substr($temp, $posStart, ($posEnd - $posStart));
        }
        $this->img = null;
        
        //Social            
        $referenceVK = 'http://api.vk.com/method/newsfeed.search?q='.$link."&count=0";
        //$referenceVK = "http://api.vk.com/method/likes.getList?type=sitepage&owner_id=".$owner_id."&page_url=".$link."&filter=copies&count=1";
        $referenceFB = 'http://api.facebook.com/restserver.php?method=links.getStats&urls='.$link."&format=json";
        
        //VK
        $ch = curl_init($referenceVK);    
        curl_setopt ($ch , CURLOPT_USERAGENT , "Chrome/40.0"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_ENCODING, 'utf-8;');
        $temp = curl_exec($ch);                
        curl_close($ch);     
        $tagStart = 'response';
        $tagEnd = ']}';
        $posStart = stripos($temp, $tagStart) + strlen($tagStart) + 3;
        $posEnd = stripos($temp, $tagEnd);      
        $this->vkRef = substr($temp, $posStart, ($posEnd - $posStart));        
        
        //FB
        $ch = curl_init($referenceVK);    
        curl_setopt ($ch , CURLOPT_USERAGENT , "Chrome/40.0"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_ENCODING, 'utf-8;');
        $temp = curl_exec($ch);                
        curl_close($ch);
        $tagStart = 'share_count';
        $tagEnd = 'like_count';                
        $posStart = stripos($temp, $tagStart) + strlen($tagStart) + 2;
        $posEnd = stripos($temp, $tagEnd) - 2;     
        $this->fbRef = substr($temp, $posStart, ($posEnd - $posStart));     
    }
}