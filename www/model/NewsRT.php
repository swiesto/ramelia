<?php
namespace model;

class NewsRT extends News
{
    public function loadNews($link)
    {
        $text = null;
        $this->source = $link;        
        $ch = curl_init($link);    
        curl_setopt ($ch , CURLOPT_USERAGENT , "Chrome/40.0"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_ENCODING, 'utf-8;');
        $text = curl_exec($ch);                
        curl_close($ch);       

        $tagStart = 'h1 itemprop';
        $tagEnd = '/strong></h1';        
        $posStart = stripos($text, $tagStart) + strlen($tagStart) + 16;
        $posEnd = stripos($text, $tagEnd) - 1;     
        $title = substr($text, $posStart, ($posEnd - $posStart));            
        $this->title = $title;       
              
        $tagStart = 'short-story';
        $tagEnd = 'article-body';
        $t1 = stripos($text, $tagStart);
        $t2 = stripos($text, $tagEnd);            
        $posStart = stripos($text, $tagStart) + strlen($tagStart);
        $posEnd = stripos($text, $tagEnd);                
        $temp = substr($text, $posStart, ($posEnd - $posStart));                            
        $tagStart = '<p>';
        $tagEnd = '</p>';                
        $posStart = stripos($temp, $tagStart) + strlen($tagStart);
        $posEnd = stripos($temp, $tagEnd);    
        $summary = substr($temp, $posStart, ($posEnd - $posStart));    
        $this->summary = $summary;            
       
        $tagStart = 'headline description';
        $tagEnd = 'class="visuallyhidden"><img src';
        $t1 = stripos($text, $tagStart);
        $t2 = stripos($text, $tagEnd);                
        if($t1 == 0 || $t2 == 0 ) {
            $this->img = null; 
        } else {               
            $posStart = stripos($text, $tagStart) + strlen($tagStart);
            $posEnd = stripos($text, $tagEnd);                
            $temp = substr($text, $posStart, ($posEnd - $posStart));                            
            $tagStart = 'url';
            $tagEnd = 'jpg';                
            $posStart = stripos($temp, $tagStart) + strlen($tagStart) + 2;
            $posEnd = stripos($temp, $tagEnd) + 3;    
            $img = 'https://russian.rt.com/'.substr($temp, $posStart, ($posEnd - $posStart));    
            $this->img = $img;       
        }
               
        //Social            
        $referenceVK = 'http://api.vk.com/method/newsfeed.search?q='.$link."&count=0";
        //$referenceVK = "http://api.vk.com/method/likes.getList?type=sitepage&owner_id=".$owner_id."&page_url=".$link."&filter=copies&count=1";
        $referenceFB = 'http://api.facebook.com/restserver.php?method=links.getStats&urls='.$link."&format=json";
        
        //VK
        $ch = curl_init($referenceVK);    
        curl_setopt ($ch , CURLOPT_USERAGENT , "Chrome/40.0"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_ENCODING, 'utf-8;');
        $temp = curl_exec($ch);                
        curl_close($ch);     
        $tagStart = 'response';
        $tagEnd = ']}';
        $posStart = stripos($temp, $tagStart) + strlen($tagStart) + 3;
        $posEnd = stripos($temp, $tagEnd);      
        $this->vkRef = substr($temp, $posStart, ($posEnd - $posStart));        
        
        //FB
        $ch = curl_init($referenceVK);    
        curl_setopt ($ch , CURLOPT_USERAGENT , "Chrome/40.0"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_ENCODING, 'utf-8;');
        $temp = curl_exec($ch);                
        curl_close($ch);
        $tagStart = 'share_count';
        $tagEnd = 'like_count';                
        $posStart = stripos($temp, $tagStart) + strlen($tagStart) + 2;
        $posEnd = stripos($temp, $tagEnd) - 2;     
        $this->fbRef = substr($temp, $posStart, ($posEnd - $posStart));     
    }
}