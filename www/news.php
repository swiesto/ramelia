<?php
    require_once 'autoloader\Autoload.php';
        
    use autoloader\Autoload;
    use view\Page;
    use model\NewsList;

    Autoload::autoloadRegister();
    
    class NewsPage extends Page
    {
        public function contentTag()
        {
        ?>
        <article>
            <h3>Новости</h3>            
            <?php            
                $newsList = new NewsList();                
                $newsList->getList();
                $newsList->sortList();
                $newsList->printList();                 
                unset($newsList);    
            ?>            
        </article>    
        <?php    
        }
    }    
    $page = new NewsPage();
    $page->displayPage();

