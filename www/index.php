<?php
    require_once 'autoloader\Autoload.php';   
    use autoloader\Autoload;
    use view\Page;
    use model\NewsRT;
    use model\NewsRG;
    use model\NewsLenta;
    Autoload::autoloadRegister();    
    class Index extends view\Page
    {
        protected function contentTag()
        {      
            $news = null;
            $source = $_POST['source'];
            if($source) {  
                if(stripos($source, 'lenta.ru/news')) {
                    $news = new NewsLenta;
                } elseif(stripos($source, 'rg.ru/')) {    
                    $news = new NewsRG;
                } elseif(stripos($source, 'russian.rt.com/article')) {                  
                    $news = new NewsRT;
                }            
           
                $news->loadNews($source);
                $news->setNews();
                $news->getNews($source);
            }
            ?>            
                <article>
                    <h3>Ramelia - aгрегатор новостей russian.rt.com, lenta.ru, rg.ru</h3>       
                    <form method='post' align="center" action='index.php' >
                        <p>
                            <strong>Введите ссылку:</strong>
                            <input type="text" size="100%" name="source"/>
                            <input type="submit" value="Найти" class="button">
                        </p>
                    </form>
                    <p>
                    <?php
                        if($source) {
                            $news->printNews();
                        }
                    ?></p>
                </article>
            <?php    
            $news = null;
            unset($news);  
        }        
    }    
    $page = new Index();
    $page->displayPage();

